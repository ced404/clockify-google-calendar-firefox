# Clockify time entry to Google Calendar

Quickly send your Clockify time entrie to your Google Calendar.

No Google Calendar API key / authentification needed!

The extension will craft an "event edit" link to Google Calendar, for example:

```
https://calendar.google.com/calendar/r/eventedit?text=Clockify%20Google%20Calendar%20Extension%20-%20Publishing&dates=20190913T134500/20190913T142300&details=%3Cstrong%3EClockify%20Google%20Calendar%20Extension%3C%2Fstrong%3E%0A%3Cul%3E%3Cli%3EPublishing%3C%2Fli%3E%3C%2Ful%3E&sf=true&output=xml
```

[Add this test event](https://calendar.google.com/calendar/r/eventedit?text=Clockify%20Google%20Calendar%20Extension%20-%20README&dates=20191004T223800/20191004T223800&details=%3Cstrong%3EClockify%20Google%20Calendar%20Extension%3C%2Fstrong%3E%0A%3Cul%3E%3Cli%3EREADME%3C%2Fli%3E%3C%2Ful%3E&sf=true&output=xml) to my calendar.


## Usage

- Install the extension
- Log in your Clockify account
- Browse to the "Time Tracker" tab: [https://clockify.me/tracker](https://clockify.me/tracker)
- A Google Calendar icon should appear on each time entries

You should be logged in your Google Calendar account.

The project name and task description will be added to Google Calendar.

You can edit the event details before insertion.

Icon: "Time" by [Rudez Studio from the Noun Project](https://thenounproject.com/rudezstudio/collection/time-glyph/?i=2405612)


