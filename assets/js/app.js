(function() {
	"use strict";
	console.log("Hello clockify!");

	const State = {
		observer: null,
		observerConfig: {
			childList: true,
			subtree: true
		},
		buttonTpl:
			'<button type="button" data-button="add2calendar" title="Add to Google Calendar"><svg xmlns="http://www.w3.org/2000/svg" width="48" height="48" viewBox="0 0 48 48"><path fill="#c7c7c7" fill-rule="evenodd" d="M38 5H10a2 2 0 0 0-2 2v3h32V7a2 2 0 0 0-2-2zM14 8a1 1 0 1 1 0-2 1 1 0 0 1 0 2zm20 0a1 1 0 1 1 0-2 1 1 0 0 1 0 2z" clip-rule="evenodd"/><path fill="#1976d2" fill-rule="evenodd" d="M44 11c.103-.582-1.409-2-2-2H6c-1 0-2.103 1.418-2 2 .823 4.664 3 15 3 15h34s2.177-10.336 3-15z" clip-rule="evenodd"/><path fill="#1e88e5" fill-rule="evenodd" d="M41 26H7S4.823 36.336 4 41c0 1.146.792 2 2 2h36c.591 0 2-.5 2-2-.823-4.664-3-15-3-15z" clip-rule="evenodd"/><path fill="#fafafa" fill-rule="evenodd" d="M20.534 26c.984.325 1.687.85 2.105 1.557.433.732.65 1.55.65 2.457 0 1.582-.519 2.826-1.556 3.733-1.037.906-2.363 1.36-3.977 1.36-1.582 0-2.892-.427-3.93-1.282-1.038-.855-1.536-2.014-1.497-3.476l.036-.072h2.242c0 .914.28 1.642.841 2.182.56.541 1.33.811 2.308.811.994 0 1.773-.27 2.337-.811.564-.541.847-1.34.847-2.397 0-1.073-.25-1.864-.751-2.373-.501-.509-1.292-.763-2.373-.763h-2.051V26h4.769zm11.103 0h2.349v8.856h-2.349z" clip-rule="evenodd"/><path fill="#e0e0e0" fill-rule="evenodd" d="M14.727 22.036h-2.254l-.024-.072c-.04-1.312.435-2.427 1.425-3.345.99-.918 2.284-1.377 3.882-1.377 1.606 0 2.886.427 3.84 1.282.954.855 1.431 2.073 1.431 3.655 0 .716-.217 1.429-.65 2.141-.433.712-1.083 1.254-1.95 1.628l.107.052h-4.77v-.911h2.051c1.042 0 1.779-.26 2.212-.781.433-.521.65-1.246.65-2.176 0-.994-.246-1.749-.739-2.266-.493-.517-1.22-.775-2.182-.775-.914 0-1.648.268-2.2.805-.534.518-.81 1.202-.829 2.14zM33.986 26h-2.349v-6.218l-3.554.048v-1.694l5.903-.644z" clip-rule="evenodd"/><path fill="#1976d2" fill-rule="evenodd" d="M6 9c-1.438 0-2.103 1.418-2 2 .823 4.664 3 15 3 15m34 0s2.177-10.336 3-15c0-1.625-1.409-2-2-2" clip-rule="evenodd"/></svg></button>'
	};

	const sendEvents = function(event) {
		// Elements
		let timeEntry = event.target.closest("time-tracker-entry");
		let project = timeEntry.querySelector(".cl-project-name");
		let inputDescription = timeEntry.querySelector(".cl-input-description");
		let inputStartTime = timeEntry.querySelector(
			".cl-single-date-picker input-time-ampm:first-child input"
		);
		let inputEndTime = timeEntry.querySelector(
			".cl-single-date-picker input-time-ampm:last-child input"
		);
		let entryGroup = timeEntry.closest("entry-group");
		let day = entryGroup.querySelector(
			"entry-group-header .cl-card-header span"
		).innerText;

		// Texts
		let entryProject = project.innerText || "Projet sans titre";
		let entryDescription = inputDescription.value;
		let entryStartTime = inputStartTime.value;
		let entryEndTime = inputEndTime.value;

		// Day
		let dayDate = null;
		// console.log("day", day);

		if (day === "Today") {
			dayDate = new Date();
		} else if (day === "Yesterday") {
			let d = new Date();
			dayDate = new Date(d.setDate(d.getDate() - 1));
		} else {
			// ex. Thu, 5th Sep
			day = day.replace("th", "");
			day = day.replace("st", "");
			day = day.replace("nd", "");
			day = day.replace("rd", "");
			let year = new Date().getFullYear();
			dayDate = new Date(day + " " + year);
			// console.log("dayDate", dayDate);
		}

		// YYYYMMDD
		let YYYY = dayDate.getFullYear().toString();
		let MM = parseInt(dayDate.getMonth() + 1);
		MM = MM < 10 ? `0${MM}`.slice(-2) : MM;
		let DD = ("0" + dayDate.getDate()).toString().slice(-2);

		// console.log("date", YYYY, "month", MM, "day", DD);
		// console.log("entryStartTime", entryStartTime);
		// console.log("entryEndTime", entryEndTime);

		// Event
		let eventName = encodeURIComponent(`${entryProject} - ${entryDescription}`);
		let eventDetails = encodeURIComponent(
			`<strong>${entryProject}</strong>` +
				"\n" +
				`<ul><li>${entryDescription}</li></ul>`
		);

		let startDate =
			`${YYYY}${MM}${DD}T` + entryStartTime.replace(":", "") + "00";
		let endDate = `${YYYY}${MM}${DD}T` + entryEndTime.replace(":", "") + "00";

		let url = `https://calendar.google.com/calendar/r/eventedit?text=${eventName}&dates=${startDate}/${endDate}&details=${eventDetails}&sf=true&output=xml`;

		console.info("Google Calendar URL", url);
		window.open(url, "_blank");
	};

	const addCalendarButton = function(timeEntry) {
		timeEntry.setAttribute("data-google-calendar", "true");
		let temp = document.createElement("div");
		temp.innerHTML = State.buttonTpl.trim();
		let buttonElement = temp.firstChild;
		buttonElement.addEventListener("click", sendEvents);
		let actions = timeEntry.querySelector(".cl-timetracker-list-actions");
		actions.appendChild(buttonElement);
	};

	const bindEvents = function() {
		// Setup Mutation Observer
		State.observer = new MutationObserver(function(mutations) {
			mutations.forEach(function(mutation) {
				// Observe path changes
				// if (State.currentPath != document.location.pathname) {
				// 	State.currentPath = document.location.pathname;
				// 	console.log("path changed!", State.currentPath);
				// 	if (State.currentPath === "/tracker") addCalendarButtons();
				// }

				// Observe <time-tracker-entry> nodes
				if (mutation.type !== "childList") return;

				if (mutation.addedNodes.length) {
					mutation.addedNodes.forEach(node => {
						if (
							node.tagName !== "TIME-TRACKER-ENTRY" ||
							node.hasAttribute("data-google-calendar")
						)
							return;
						addCalendarButton(node);
					});
				}
			});
		});

		// https://davidwalsh.name/mutationobserver-api
		State.observer.observe(document.body, State.observerConfig);
	};

	// Init
	const init = function() {
		State.currentPath = window.location.pathname;
		window.onload = bindEvents;
	};

	init();
})();
